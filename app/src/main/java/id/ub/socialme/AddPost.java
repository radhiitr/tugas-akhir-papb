package id.ub.socialme;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class AddPost extends AppCompatActivity {
    FirebaseDatabase fbdbs = FirebaseDatabase.getInstance();
    DatabaseReference refRoot = fbdbs.getReference();
    DatabaseReference refStatus = refRoot.child("status");
    EditText etStatus;
    Button btAdd, logout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_post);
        etStatus = findViewById(R.id.etAdd);
        btAdd = findViewById(R.id.btAdd);
        btAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle info = getIntent().getExtras();
                String status = etStatus.getText().toString();
                String username = info.getString("username");
                String idu = info.getString("id");
                Status s = new Status();
                s.setUsername(username);
                s.setIdUser(idu);
                s.setStatus(status);
                String key = refStatus.push().getKey();
                s.setIdStatus(key);
                refStatus.child(key).setValue(s);
                Intent i = new Intent(AddPost.this, Home.class);
                startActivity(i);
            }
        });

        logout = findViewById(R.id.btLogout);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v.getId() == R.id.btLogout) {
                    FirebaseAuth.getInstance().signOut();
                    Intent so = new Intent(AddPost.this, SignIn.class);
                    startActivity(so);
                }
            }
        });





    }
}