package id.ub.socialme;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity {

    TextView sm;
    private FirebaseAuth mAuth;
    FirebaseUser currentUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /* currentUser = mAuth.getCurrentUser();
        if(currentUser!=null)
        {
            if (currentUser.isEmailVerified()) {
                Intent home = new Intent(this, Home.class);
                home.putExtra("email", currentUser.getEmail());
                startActivity(home);
            }
        } */

        Intent intent = new Intent(this, SignIn.class);
        startActivity(intent);
        finish();
    }
}