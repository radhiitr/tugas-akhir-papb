package id.ub.socialme;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class EditPost extends AppCompatActivity {
    EditText etstatus;
    String idstatus, status, username, iduser;
    Button btEdits, logout;
    FirebaseDatabase fbdbs = FirebaseDatabase.getInstance();
    DatabaseReference refRoot = fbdbs.getReference();
    DatabaseReference refStatus = refRoot.child("status");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_post);

        Intent ed = getIntent();
        idstatus = ed.getStringExtra("idstatus");
        status = ed.getStringExtra("status");
        username = ed.getStringExtra("username");
        iduser = ed.getStringExtra("iduser");

        etstatus = findViewById(R.id.etEdit);
        etstatus.setText(status);

        btEdits = findViewById(R.id.btEdits);
        btEdits.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Status s = new Status();
                s.setUsername(username);
                s.setIdUser(iduser);
                s.setStatus(etstatus.getText().toString());
                s.setIdStatus(idstatus);
                refStatus.child(idstatus).setValue(s);
                Intent i = new Intent(EditPost.this, Home.class);
                startActivity(i);
            }
        });

        logout = findViewById(R.id.btLogout);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v.getId() == R.id.btLogout) {
                    FirebaseAuth.getInstance().signOut();
                    Intent so = new Intent(EditPost.this, SignIn.class);
                    startActivity(so);
                }
            }
        });


    }
}