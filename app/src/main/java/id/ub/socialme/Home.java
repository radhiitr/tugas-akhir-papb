package id.ub.socialme;

import static android.content.ContentValues.TAG;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.nfc.Tag;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

public class Home extends AppCompatActivity {
    private FirebaseAuth mAuth;
    FirebaseUser currentUser;
    Button logout, addpost;
    RecyclerView rvstatus;
    TextView tv;
    String b,c;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        rvstatus = findViewById(R.id.rvStatus);
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        b = user.getDisplayName();
        rvstatus.setLayoutManager(new LinearLayoutManager(this));
        FirebaseDatabase fbdbu = FirebaseDatabase.getInstance();
        DatabaseReference refu = fbdbu.getReference().child("user");
        refu.orderByChild("username").equalTo(b).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for (DataSnapshot child: snapshot.getChildren()) {
                    c = child.getKey();
                    Query query = FirebaseDatabase.getInstance().getReference().child("status").limitToLast(50);
                    FirebaseRecyclerOptions<Status> options = new FirebaseRecyclerOptions.Builder<Status>()
                            .setQuery(query, Status.class).build();

                    FirebaseRecyclerAdapter<Status, StatusViewHolder> adapter = new FirebaseRecyclerAdapter<Status, StatusViewHolder>(options) {
                        @NonNull
                        @Override
                        public StatusViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row, parent, false);
                            return new StatusViewHolder(view);
                        }

                        @Override
                        protected void onBindViewHolder(@NonNull StatusViewHolder holder, int position, @NonNull Status model) {
                            holder.setStatus(model);
                            holder.btDetail.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent dt = new Intent(Home.this, Detail.class);
                                    dt.putExtra("username", holder.uname2);
                                    dt.putExtra("status", holder.statuss2);
                                    dt.putExtra("id", c);
                                    startActivity(dt);
                                }
                            });
                        }
                    };

                    adapter.startListening();
                    rvstatus.setAdapter(adapter);

                    addpost = findViewById(R.id.btAddPost);
                    addpost.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent add = new Intent(Home.this, AddPost.class);
                            Bundle info2 = new Bundle();
                            info2.putString("username", b);
                            info2.putString("id", c);
                            add.putExtras(info2);
                            startActivity(add);
                        }
                    });


                    logout = findViewById(R.id.btLogout);
                    logout.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (v.getId() == R.id.btLogout) {
                                FirebaseAuth.getInstance().signOut();
                                Intent so = new Intent(Home.this, SignIn.class);
                                startActivity(so);
                            }
                        }
                    });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });



    }

    public static class StatusViewHolder extends RecyclerView.ViewHolder {
        View view;
        TextView uname;
        TextView statuss;
        Button btDetail;
        String statuss2, uname2;

        public StatusViewHolder(@NonNull View itemView) {
            super(itemView);
            view = itemView;
        }

        public void setStatus(Status status) {
            uname = view.findViewById(R.id.tvUsername);
            statuss = view.findViewById(R.id.tvStatus);
            btDetail = view.findViewById(R.id.btDetail);
            uname.setText(status.getUsername());
            statuss.setText(status.getStatus());
            statuss2 = status.getStatus();
            uname2 = status.getUsername();
        }
    }

}